
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//1. Создать два массива students и points и заполнить их данными из studentsAndPoints без использования циклов. В первом массиве фамилии студентов, во втором баллы. Индексы совпадают.
	var students 	= studentsAndPoints.filter(   function(value, i) { return i % 2 === 0; 	} );
	var points 		= studentsAndPoints.filter(   function(value, i) { return i % 2; 		} );

//2. Вывести список студентов без использования циклов в следующем виде: Список студентов: Студент N набрал N баллов
	console.log('Список студентов:');
		students.forEach( function(value, i) { 
			console.log( "Студент " + value + ' набрал ' + points[i] + ' баллов' );
		});

//3. Найти студента набравшего наибольшее количество баллов, и вывести информацию в формате: Студент набравший максимальный балл: Алексей Левенец (70 баллов)
	var max, idx;
	points.forEach( function(value, i) {    if ( !max || value>max ){ max = value; idx = i; }   });
	console.log( "Студент набравший максимальный балл: " + students[ idx ] + ' (' + max + ' баллов)' );

//4. Используя метод массива map увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30.
	var upScore = 30;
	var newPointsStudents = ['Ирина Овчинникова', 'Александр Малов'];
	//students.map( function(name) {  if ( name === 'Ирина Овчинникова' || name == 'Александр Малов' ){  points[ students.indexOf(name) ] += upScore; } }); 
	/*	
	var newPointsStidents = ['Ирина Овчинникова', 'Александр Малов'];
		students.map( function(name) {
			newPointsStidents.forEach( function(value) {
				if ( name === value ){
					points[ students.indexOf(name) ] += upScore;
				}
			});	
		});
	*/
	
//Доработка
/*Комментарий: 
	При увеличении баллов вы используете map как обычный forEach. 
	Вам нужно не игнорировать массив который возвращает map. 
	Просто нужно чтобы эта функция вернула массив увеличенных баллов.*/
	var increasedPoints = newPointsStudents.map( function(name) {
		return points[ students.indexOf(name) ] += upScore;
	});
	
	points.forEach( function(value, i) { 
			console.log( "Студент " + value + ' --->>>>>>> ' + points[i] + ' баллов' );
		});
		
/*Дополнительное задание: реализовать функцию getTop, которое принимает на вход число и возвращает массив в формате studentsAndPoints, в котором соответствующее кол-во студентов с лучшими баллами в порядке убывания кол-ва баллов.*/	
//Доработка
/*
2. У вас получился массив массивов, т.к. splice возвращает массив всегда:
topStudentsAndPoints.push(students.splice(idx, 1));
topStudentsAndPoints.push(points.splice(idx, 1));
3. И судя по тому что вы используете splice из оригинальных массивов, то функция у вас правильно сработает только один раз. Создайте копию с использованием slice.
*/
	function getTop(counter) {
		console.log( 'Топ '+counter+':' );
		var stop = points.length;//блок на некорретный счетчик
		var topStudentsAndPoints = [];
		var topCurrentPoints = points.slice(); //Передаем значения новому массиву, который будет использоваться для сравнения максимумов
		// используем цикл, т.к. оперируем пользовательским количеством итераций	
		for (i=0; i<counter; i++) {
		if ( i >= stop ) { console.log('запрашиваемый ТОП Превышает количество студентов'); 	break; }
			var max, idx;
			topCurrentPoints.forEach( function(top, i) {    
				if ( !max || top>max ){ 
					max = top; 
					idx = i;
				}   
			});
		max = 0; //обнуление найденного максимума
		delete topCurrentPoints[idx];
		console.log( students[idx] + ' - ' + points[idx] + ' баллов' );
		topStudentsAndPoints.push(students[idx], points[idx]);
		}
	return topStudentsAndPoints;
	}
	
	getTop(5);
	getTop(3);
	getTop(7);